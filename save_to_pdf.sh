#!/bin/bash

# Replace with the path to your local sitemap.xml file
SITEMAP_FILE="public/sitemap.xml"
OUTPUT_PATH="./public/"

# Use grep to extract the URLs from the sitemap.xml file
URLS=$(grep -oP '<loc>\K.*?(?=</loc>)' "$SITEMAP_FILE")

# Loop through the URLs and do something with each one
for URL in $URLS; do
  echo "Processing URL: $URL"
  TITLE=$(curl -s "$URL" | grep -oP '<title>\K.*?(?=-)')
  TITLE="${TITLE%?}"
  google-chrome --headless --no-pdf-header-footer --print-to-pdf="$OUTPUT_PATH$TITLE.pdf" $URL
done