output "lb_dns_name" {
  value = var.enable_load_balancer ? aws_lb.main[0].dns_name : null
}

resource "local_file" "push_commands" {
  content = templatefile("templates/push_commands.tftpl", {
    repository_url = aws_ecr_repository.main.repository_url
    registry_id    = aws_ecr_repository.main.registry_id
    name           = aws_ecr_repository.main.name
    region         = data.aws_region.current.name
    cluster_name   = aws_ecs_cluster.main.name
    service_name   = aws_ecs_service.main.name

  })
  filename        = "${path.module}/push_commands.sh"
  file_permission = "0775"
}