# 22 Versionsverwaltung mit Git

!!! abstract "Zielsetzung"
    Sie erstellen einen *GitLab Account*, kopieren das *Ref-Card-03 Projekt*, erstellen ein *Dockerfile*, verfassen eine informative `README.md` und exkludieren nicht gewünschte Dateien mit einer `.gitignore`.

Versionsverwaltungssysteme werden verwendet, um Änderungen an Dateien zu verfolgen. Dabei werden die Dateien in sogenannten Repositories (engl. Behälter, Aufbewahrungsorte) abgelegt. Sie hatten in vorhergehenden Modulen bereits mit Git gearbeitet, wir beschränken uns daher in diesem Modul auf die Anwendung.

!!! task "Erstellen Sie ein Cheat-Sheet"
	In diesem Kapitel starten Sie mit der Bedienung von git. Sie werden vorwiegend mit der CLI-Variante arbeiten. Erarbeiten Sie sich ein Cheat-Sheet der wichtigsten Commands, die Sie verwenden.

## Übersicht des Auftrags
1. Erstellen Sie sich einen [Gitlab-Account](#gitlab-repository) (falls noch nicht geschehen)!
1. Forken Sie das Quellprojekt!
1. Erstellen Sie ein [Dockerfile](#) für das Spring Boot Projekt!
1. Erstellen Sie ein informatives [`README.md`](#readmemd) für das Projekt!
1. Lassen Sie sich eine [`.gitignore`](#gitignore) Datei generieren!
1. Schreiben Sie sinnvolle [Commit-Messages](#commit-strategien) für diese Dateien! Vergessen Sie den `push` ins Remote Repository nicht!

## Gitlab Repository

1. Erstellen Sie sich einen *Account* bei [GitLab](https://gitlab.com/users/sign_up).

	!!! info
		Verwenden Sie Ihre BBW-Adresse *vorname.nachname@lernende.bbw.ch* für das Login.

1. Erstellen Sie einen Fork des [bbwrl/RefCard-01-Projekt](https://gitlab.com/bbwrl/m347-ref-card-01)

	![image-20230416134913203](./assets/image-20230416134913203.png)

	!!! info "Fork"
		Ein *Fork* ist eine exakte Kopie des Ursprungsprojekts. Ab dann entwickelt es sich unabhängig; man entfernt, verändert oder ergänzt den Quellcode so, wie man es für nötig hält. Es ist möglich, aber eher unwahrscheinlich, dass ein Fork wieder mit seinem Ursprungsprojekt zusammengeführt wird.

1. Clonen Sie das Repository in Ihre lokale Arbeitsumgebung

	```{ .sh .no-copy }
	git clone https://gitlab.com/...
	```

1. Fügen Sie Ihr im [Kapitel 13](../docker/springboot.md) erstelltes `Dockerfile` ein.

### README.md

Jedes Repository sollte ein `README.md` enthalten. Der Zweck eines README ist es, folgende drei Fragen innert kürzester Zeit zu beantworten:

1. Was soll mit diesem Projekt erreicht werden? (Kurze Beschreibung)
2. Kann ich es benutzen? (Was sind die Voraussetzungen?)
3. Wenn ja, wie? (Wie kann ich es installieren? Was sind die Code-Snippets, die sofort funktionieren?)

Solche README-Dateien werden mit [Markdown](https://www.heise.de/mac-and-i/downloads/65/1/1/6/7/1/0/3/Markdown-CheatSheet-Deutsch.pdf) geschrieben (daher die Dateiendung `.md`). Ähnlich wie HTML ist Markdown eine *Auszeichnungssprache*, eine maschinenlesbare Sprache für die Gliederung und Formatierung von Texten.


1. **Projektbeschreibung**

	Bauen Sie die Projektbeschreibung mit folgenden Textbausteinen zusammen, wobei die Begriffe in den eckigen Klammern als Platzhalter dienen:

	`<project_name>` is a `<utility/tool/feature>` that allows `<insert_target_audience>` to do `<action/task_it_does>`.

	Beispiele:
	!!! quote "[Tensorflow](https://www.tensorflow.org/overview/)"

		TensorFlow makes it easy for beginners and experts to create machine learning models. See the sections below to get started.

	!!! quote "[PyTorch](https://pytorch.org/)"

		An open source machine learning framework that accelerates the path from research prototyping to production deployment.

	!!! quote "[Kubernetes](https://kubernetes.io/)"

		Kubernetes is an open-source system for automating deployment, scaling, and management of containerized applications.
	
2. **Voraussetzungen**

	Der Abschnitt Voraussetzungen besteht aus drei Kategorien.

	- **Betriebssystem**: Das Betriebssystem kommt zuerst, weil es für die meisten Benutzer vorgegeben ist. Falls ein Projekt ausschliesslich auf macOS läuft, gibt es keinen Grund für einen Windows-Benutzer, sich noch weiter mit diesem Projekt zu befassen.
	- **Framework Infrastruktur**: Frameworks stellen auch ein potenzielles Kompatibilitätsproblem dar. Falls das Projekt ausschliesslich mit Python 2 funktioniert, muss sich der Benutzer entscheiden, ob er diesen erheblichen Aufwand betreiben will, um das Projekt zu nutzen.
	- **Wissen**: Wie viel Wissen muss ich zur Nutzung dieses Projekts aneignen?

3.  **Installation und Verwendung**

	- Einfache Copy-Paste Installationsanweisung. Sollte in unter 5 Minuten umsetzbar sein und keine komplexen Abhängigkeiten aufweisen.
	- Die Verwendung des Projekts anhand möglichst vielen Beispiele demonstrieren. Das erste Beispiel sollte im Stil eines "hello-world" trivial gehalten werden. Weitere Beispiele können weiterführende, komplizierte Anwendungsfälle zeigen.

!!! tip "Gute README.md Vorlage"
	[scottydocs/README-template.md: A README template for anyone to copy and use.](https://github.com/scottydocs/README-template.md)

!!! task "Umsetzung"
	Ergänzen Sie also Ihr Projekt mit einem `README.md`.

### .gitignore

Nicht jede Datei in Ihrem Projekt sollte von Git nachverfolgt werden. Temporäre Dateien aus Ihrer Entwicklungsumgebung, Testausgaben und Logs sind Beispiele für Dateien, die wahrscheinlich nicht ins git gehören.

!!! task "Umsetzung"
	Ergänzen Sie also Ihr Projekt mit einer `.gitignore`.

	Hilfreich ist auch der .gitignore-Generator: [gitignore.io](https://www.gitignore.io). Speisen Sie ihn einmal probeweise mit `Terraform`.

## Git Commit / Push

Sobald Sie ihre ersten Dateien im Ordner hinzugefügt haben, sollten Sie die Änderungen commiten und in Ihr Remote Repository pushen.

![Git - Working area](./assets/git-working-area.png)

Ob Sie dies im Terminal oder in Visual Studio Code umsetzen, ist Ihnen überlassen. Zu Beginn ist das Modul *Source Control* in Visual Studio Code jedoch sehr zu empfehlen. Es ermöglicht Ihnen mittels einer grafischen Oberfläche alle Ihre Änderungen auf einen Blick zu sehen. Sie benötigen die Kompetenz beides zu machen.

![Visual Studio Code - Source Control](./assets/vscode-source-control.png)

!!! info "Commit Strategien"
	Es kann hilfreich sein, sich einen Commit als Checkpoint oder Savepoint für Ihr Projekt vorzustellen. In vielen Videospielen werden Checkpoints erreicht und der Fortschritt gespeichert, nachdem eine bestimmte Handlung oder Herausforderung gemeistert wurde. In ähnlicher Weise wird ein Git-Commit in der Regel durchgeführt, nachdem Sie einen Meilenstein erreicht haben und Sie Ihren Fortschritt speichern möchten. 

	Beim Verfassen von Git-Commits sollten Sie ein paar bewährte Praktiken einhalten:

	- Wählen Sie den *Imperativ* (Befehlsform) - "Halten Sie", "Steigen Sie ein", Schnallen Sie sich an"
	`fix`, `add`, `remove`, `change`
	- Halten Sie sich *kurz*: Eine commit-Nachricht sollte nicht länger als *50 Zeichen* sein.
	- Versetzen Sie sich in sich selbst: Welche *Informationen* sind nötig um diesen Commit zu verstehen? 

	!!! success "Gutes Beispiel"
		```
		fix issue with login button not showing
		```

	!!! failure "Schlechtes Beispiel"
		```
		fixed bug
		```

!!! success "Zusammenfassung"
	In Ihrem persönlichen GitLab-Account liegt ein Fork des *Ref-Card-01* Projekts. Zum bereits vorhandenen Sourcecode und den Maven-Dateien, haben Sie sich ein `Dockerfile`, `README.md` sowie eine `.gitignore` erstellt. Diese drei Dateien haben Sie erfolgreich zu Ihrem Repository hinzugefügt und mit einer sinnvollen Commit-Nachricht versehen.

## Weiterführende Links

- [Git Pro - Book](https://git-scm.com/book/en/v2)
- [README best practices. Don’t spend 2 hours on your next… | by Tianhao Zhou | Medium](https://tianhaozhou.medium.com/readme-best-practices-7c9ad6c2303)
- [Git Best Practices – How to Write Meaningful Commits, Effective Pull Requests, and Code Reviews](https://www.freecodecamp.org/news/git-best-practices-commits-and-code-reviews/)
- [Write Better Commits, Build Better Projects | The GitHub Blog](https://github.blog/2022-06-30-write-better-commits-build-better-projects/)