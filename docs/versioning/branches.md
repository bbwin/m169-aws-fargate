# 23 Git Branches

!!! abstract "Zielsetzung"

    Wenn wir zusammen am gleichen Werk arbeiten, müssen wir manchmal auch in Varianten arbeiten. Diese Varianten bezeichnen wir als Branch und erlauben, dass ein Projekt sich weiter entwickelt, ohne an der bestehend funktionierenden Variante etwas zu ändern. Anders als beim Fork geht es aber darum, die Varianten später wieder zusammen zu führen.

## Theorie

Sie finden die Theorie zu diesem Thema im [Git Pro Book, Kapitel 3](https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell). Verschaffen Sie sich einen Überblick über das Kapitel innert 15 Minuten, mit dem Ziel grundsätzliches Verständnis über das Branching zu kriegen und welche Themen in etwa wo zu finden sind.

Jetzt sollten Sie folgende Fragen beantworten können:

<details>
<summary>- Wofür sind Branches gut?</summary>
Branches erlauben einen neuen Zweig zu starten, ohne den ursprünglichen zu verändern.
</details>

<details>
<summary>- Worin unterscheidet sich der Branch vom Fork?</summary>
Während bei Fork ein neues Projekt gestartet wird, bleiben wir mit dem Branch im gleichen Projekt.
</details>

<details>
<summary>- Welches Ziel verfolgen wir beim Branch?</summary>
Das Ziel lautet dabei entweder unser Projekt mit dem neuen Branch weiterzuführen oder die Anpassungen wieder in den ursprünglichen Zweig zurückzuführen.
</details>

## Auftrag
Das wollen wir natürlich selbst ausprobieren und testen.

1. git clone eines Repository eines Freundes
1. Erstellen eines Branch
1. Ändern von Inhalten
1. Merge request und pull durchführen

### Anpassungen in neuem Branch vornehmen

Starten Sie in einem Verzeichnis mit einer frischen Kopie eines Projekts (`git clone`). Empfehlenswert ist dafür z. B. der eigene vorgängig erstellte Ref Card 01 Fork. Noch spannender wird es, wenn Sie das Projekt Ihres Tandempartners clonen - bzw. gegenseitig.

Wir wollen nun einige Daten davon abändern:

1. Erstellen Sie eine neue Datei `author.md` und tragen Sie Ihre beiden Namen ein.
1. Fügen Sie diese Datei nun zum git dazu (`git add`).
1. Ergänzen Sie im README.md die Versionsnummer von Java.
1. Erstellen Sie einen neuen Branch, damit Sie die ursprüngliche Daten Ihres Kollegen nicht stören.
1. Sie müssen die Änderungen natürlich bestätigen (`git commit`).
1. Ihr Tandem muss Ihnen mind. Developer-Rechte auf sein Projekt geben.
1. Jetzt gilt es, den Branch ins git zu laden (`git push`).
1. Sie können als Letztes noch ein merge request durchführen.

### Anpassungen annehmen

Ihr Tandem hat Ihnen nun ein Branch erstellt. Gehen wir davon aus, dass Sie mit den Änderungen einverstanden bzw. sogar froh darum sind. Sie werden also Änderungen davon übernehmen. Den Schwierigkeitsgrad wählen Sie selbst. Hier einige Ideen und Varianten:

- Sie übernehmen einfach alles und führen den Branch zusammen.
- Sie gehen die Änderungen einzeln durch und führen nur ein Teil davon zusammen.
- Sie machen noch andere Änderungen an getrennten Stellen und führen die Änderungen zusammen.
- Sie ändern an denselben Stellen, was zu Konflikten führen wird, und versuchen diese aufzulösen.

Fügen Sie Ihrem Lernjournal den verwendeten Weg bei und halten Sie Ihre Erkenntnisse fest!

Es ist nicht Ziel des Moduls, `git` bis ins letzte Detail anwenden zu können. Ihrer Lernfreude möchten wir aber nicht im Weg stehen. Viel Erfolg!

!!! task "Vergleichen des Cheat-Sheets"
    Sie haben am Anfang dieses Kapitels ein CheatSheet zu git gestartet. Das Linux-Magazin publizierte in der Ausgabe vom Juli 2024 ebenfalls ein solches. Vergleichen Sie Ihre Variante mit dieser. Erforschen Sie diejenigen Themen, die Sie hier noch nicht einmal streiften.

!!! success "Zusammenfassung"
	`git branch` ist ein wichtiges und nützliches Werkzeug in `git` und der Zusammenarbeit. In dieser Übung haben Sie sich mit dem Konzept und der Anwendung auseinander gesetzt. Womöglich haben Sie sogar Konflikte aufgelöst und sich über den Aufwand dabei geärgert. Kleine Commits verhindern komplexe Konflikte.

## Weiterführende Links

- [Git Pro - Book](https://git-scm.com/book/en/v2)