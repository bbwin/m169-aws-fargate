# 44 Aktualisiertes Docker Image

!!! abstract "Zielsetzung"

    - Sie aktualisieren Ihr Docker Image und können Ihre neue Version in AWS ECS erneuern.
    - Sie lösen das IP-Problem in AWS ECS

## Aktualisierte Website

Sie stören sich wiederum an der Hintergrundfarbe Ihrer Website. Fügen Sie zudem Text und Bild ein, um die Site etwas hübscher zu gestalten.

Sorgen Sie dafür, dass die neue Website online geht.

In Ihrem privaten Repository in *AWS ECR* finden Sie nun zwei Images.
![image-20230328174958641](./assets/image-20230328174958641.png)

!!! task "Schau genau hin"
	Falls Sie die richtigen Schritte unternommen haben, ändert sich ihre Public IP!
    
    Wie lösen Sie das Problem mit den sich ständig wechselnden IP-Adressen?

## Spring Boot Applikation in Fargate laufend

Die statische Website mit nginx/index.html ist ja ganz hübsch. Aber die Applikationsentwickler kommen mit grösseren Geschützen daher. Deshalb versuchen wir das Gelernte auch mit der SpringBoot-Applikation nachzubauen.

Wie gehen Sie vor? Verfassen Sie ein Ablaufschema (Struktogramm? UML Aktivitätendiagramm?) um den Vorgang schematisch zu erfassen und sich Klarheit zu verschaffen.

Erst danach sorgen Sie für eine lauffähige Spring Boot Website in Fargate.

!!! tip "Dokumentation"
	Falls Sie nun zurück in zu *AWS ECR* gehen, um die Befehle zu kopieren, sollten Sie spätestens jetzt beginnen, Ihre Schritte an einem Ort Ihrer Wahl zu dokumentieren. Ob OneNote, Markdown Editor oder in einem Word Dokument ist Ihnen überlassen, aber Sie sparen sich so wertvolle Zeit und es hilft Ihnen beim Lernen.