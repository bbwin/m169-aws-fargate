# 41 Einleitung

!!! abstract "Lernziel"

    In diesem Abschnitt lernen Sie, eine containerisierte Applikation in Amazon Elastic Container Registry hochzuladen und in Amazon Elastic Container Service bereitzustellen.

| Produkte                                                     | AWS Service                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![WSL2](./assets/ubuntu.png) WSL2 Ubuntu Terminal | ![Arch_Amazon-Elastic-Container-Registry_32](./assets/ecr.png) Elastic Container Registry |
| ![Docker](./assets/docker.png) Docker Desktop mit WSL2 Integration | ![Arch_Amazon-Elastic-Container-Service_32](./assets/ecs.png) Elastic Container Service |
| ![VSCode](./assets/vscode.png) Visual Studio Code mit [Remote Development extension pack](https://aka.ms/vscode-remote/download/extension) | ![Arch_Elastic-Load-Balancing_32](assets/elb.png) Elastic Load Balancing |

Wir haben im letzten Abschnitt Docker Container als standardisierte Umgebung kennengelernt, die isolierende Gefässe für Applikationen ermöglicht. Diese wollen wir aber nicht lokal betreiben, sondern idealerweise in einem Cluster einsetzen. Und zwar skalierbar: Vergrössert sich die Last, wollen wir problemlos weitere Instanzen hoch fahren.

Dafür setzen wir AWS Fargate ein. Wir geben unser zu betreibendes Image in eine Registry und lassen es im Container Service laufen. Die Automatisierungsregeln bestimmen wir in der Task Definition.

1. Hochladen des Images in Amazon Elastic Container Registry (ECR)
1. Setup eines Amazon Elastic Container Service
1. Erstellen einer Amazon ECS Task Definition, um Docker Container auszuführen
1. Bereitstellen des Containers mittels Service