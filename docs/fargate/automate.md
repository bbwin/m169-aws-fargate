# 45 Automat mit 3-Tier-Applikation

!!! abstract "Lerninhalt"
    Setzen Sie alles bisher gelernte zusammen, fügen noch Komplexität hinzu und erfreuen Sie sich an einem realen Beispiel. Wir arbeiten mit der RefCard-03, der Jokes-DB!

## Aufgabenstellung

Sie haben in [14 Multi-Container-Applikation](../docker/docker-compose.md) bereits die RefCard-03 Jokes-DB geforkt und dann per `docker-compose` die Applikation lokal in Betrieb genommen. Nun möchten wir diese in AWS Fargate betreiben. Und zwar so, dass wenn im gitlab-Repository eine Anpassung stattfindet, automatisch die Applikation neu zur Verfügung steht.

## Vorgehen

1. Erstellen Sie eine GitLab Pipeline
1. Bauen Sie eine Stage Release in Ihre Pipeline
1. Erstellen Sie die CI/CD Variablen
1. Testen Sie Ihre Pipeline

### Komplexere GitLab Pipeline für RefCard03

Wir erstellen zum Projekt eine `.gitlab-ci.yml`.

```yaml title=".gitlab-ci.yml"
--8<-- "ref-card-03/.gitlab-ci.yml"
```

!!! info "Docker in Docker"

    Diese Pipeline ist bereits etwas komplexer und benutzt das GitLab CI/CD, um Docker Images mit Docker zu builden - auch DinD (Docker in Docker) genannt. Der erste Abschnitt mit `image:`, `variables:` und `services:` sind Voraussetzung und Sie sollten diese 1:1 so übernehmen. Sie finden weiterführende Informationen dazu unter [Use Docker to build Docker images](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html).


!!! task "Schau genau hin"
    Die Befehle unter `before_script:` und `script:` sollten Ihnen aber sehr bekannt vorkommen. Studieren Sie die Befehle! Welche sind neu, welche angepasst?

Commiten und pushen Sie diese aktualisierte Datei und überprüfen Sie den Status. Zwei wichtige Teile fehlen noch: die benutzten *Variablen* und die *AWS Infrastruktur*.

### CI/CD Variablen

Wir benutzen in dieser YAML-Datei mehrere nicht definierte Variablen. Erfassen Sie diese in GitLab unter *Settings* &rarr; *CI/CD* &rarr; *Variables*.

![image-20230510091802691](./assets/gitlab-variables.png)

Die Zugangsdaten finden Sie im Learner Lab unter *AWS Details* &rarr; *AWS CLI*.

![image-20230510092304455](./assets/learnerlab-credentials.png)

!!! warning "Kurzlebige Secrets"

    Die Zugangsdaten im AWS Academy Learner Lab sind kurzlebig und erneuern sich bei jedem Neustart der AWS-Umgebung. Sie müssen diese Daten jeweils neu vom *Learner Lab* zu Gitlab übertragen, wenn Sie Ihr Lab starten. Dies ist wegen dem Learner Lab so - in der Realität verändern sich diese Werte nicht. Dafür benötigen Sie eine Kreditkarte und es fallen auch Kosten an.

Die weiteren Variablen sind von Ihrer Umgebung in AWS abhängig. Erstellen Sie eine ECR Registry (dies kennen Sie bereits aus vergangenen Aufträgen) und befüllen Sie die Variablen mit Ihren Werten:

- AWS_DEFAULT_REGION
- CI_AWS_ECR_REGISTRY
- CI_AWS_ECR_REPOSITORY_NAME

Starten Sie die Pipeline manuell aus GitLab und verfolgen Sie den Build-Prozess. Überprüfen Sie das erfolgreiche pushen in Ihr neu erstelltes ECR Repository.

![image-20230510092304451](./assets/gitlab-pipeline-pushed-to-ecr.png)

## Deployment einrichten

Mit den Schritten bis hierhin haben Sie nun eine Pipeline, die bei jedem Push in das Repository, das Image neu bildet und in ECR bereitstellt. Als letzter Schritt fehlt noch das automatische Deployment in ECS. Erstellen Sie hierfür in der Pipeline eine weitere Stage *auto-deploy* und versehen Sie diese mit den nötigen Befehlen, um mit der *aws-cli* das Deployment zu automatisieren. Vergessen Sie nicht, dass Sie zuvor (wie in vergangenen Aufträgen gelernt) den für den Betrieb nötigen Task, Service und Cluster im AWS Backend noch erstellen müssen.  

Aktualisieren Sie danach Ihr Readme.md mit allen neuen Informationen, so dass Ihr Repository (nach den IaC-Prinzipen) selbsterklärend ist. Nutzen Sie gerne auch Unterseiten, damit das README nicht zu lang wird.

!!! task "Test Vollautomat"
    Gewünscht ist eine neue Hintergrund-Farbe der Jokes-DB-Applikation. Passen Sie deshalb in Ihrem Clone lokal die Datei `src/main/resouces/templates/index.html` an. Mit dem push ins gitlab soll die Applikation automatisiert neu deployed und online erreichbar sein.

## Rückblende

Verwenden Sie ein Stift und ein Blatt Papier und zeichnen Sie mit Ihrem Tandem grafisch auf, was Sie wie nun automatisiert haben und wie der ganze Ablauf aussieht. Starten Sie beim Java-Code, der in der Ref-Card-03 verarbeitet wird, bis zum fertig ausgelieferten Dienst, der in der AWS-Umgebung läuft. Zeichnen Sie schematisch auf, welche Daten durch welche Prozesse auf welcher Infrastruktur einflossen, um das Ergebnis letztlich zu erreichen. Diskutieren Sie auch die verwendete Symbolik! Tauschen Sie sich im KOPING (Tandem-Tandem) aus, um weitere Ideen zur Darstellung zu erlangen!
