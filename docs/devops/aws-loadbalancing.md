# 53 Load Balancing

!!! abstract "Kompetenzen"

    - Services administrieren und überwachen
    - bzw. aus der [Modulidentifikation](https://www.modulbaukasten.ch/module/169/1/de-DE?title=Services-mit-Containern-bereitstellen) Handlungsziel 7: Administriert und überwacht die bereitgestellten Services.

## AWS CloudWatch

Falls noch nicht geklärt: Fragen Sie Ihre Lehrperson nach dem Zugang zum AWS Cloud Foundation Kurs. In diesem suchen Sie dann im Modul 10 - Auto Scaling and Monitoring das Lab 6 - Scale & Load Balance your Architecture. Dieses spielen Sie hier durch.

## Einstiegsszenario

Mit etwas Glück wird Ihnen Ihre Lehrperson das Einstiegsszenario mündlich näher bringen und erklären, wie dieses aufgebaut ist.

![starting-architecture](./assets/starting-architecture.png)

Dieses passen Sie nun an, um folgendes Szenario zu erhalten:

![final-architecture](./assets/final-architecture.png)

Dazu haben Sie nun mehrere Varianten:

<details>
<summary>1. Sie schalten sofort Ihr Hirn aus und befolgen stur die Arbeitsanweisungen im Kurs.</summary>
Dieser Weg führt zum Ziel. Sie werden das Ergebnis erleben und sich darüber erfreuen können. Leider werden Sie damit niemals fähig sein, eine ähnliche Aufgabe selbst lösen zu können. Wohl nicht mal dieselbe...
</details>

<details>
<summary>2. Sie lesen (bzw. schreiben raus?) die Schritte in der Arbeitsanweisung durch und versuchen diese zu vollziehen.</summary>
Tolle Idee, falls Sie schon Erfahrung mit AWS haben. Also zum Beispiel bereits ein paar Mal Variante 1 wählten. Womöglich ist die Übung aber bereits zu schwierig, um direkt mit dieser Variante einzusteigen. Sie werden sich im AWS-Dschungel verirren.
</details>

<details>
<summary>3. Sie haben ewig Zeit und versuchen's selbst.</summary>
Abenteurer! Der Weg ist das Ziel. Sie haben's verstanden. Das Ende werden Sie zwar nie erreichen, werden aber dabei unheimlich viele Erfahrungen sammeln. Ihr Leben wird bereichert weitergehen.
</details>

<details>
<summary>Wahlentscheidung</summary>
Ich empfehle Ihnen statt der Entscheidung zu einer Variante, alle drei in genau dieser Reihenfolge durchzuführen.
</details>