# 51 Einleitung Monitoring

!!! abstract "Kompetenzen"

    - Services administrieren und überwachen
    - bzw. aus der [Modulidentifikation](https://www.modulbaukasten.ch/module/169/1/de-DE?title=Services-mit-Containern-bereitstellen) Handlungsziel 7: Administriert und überwacht die bereitgestellten Services.

## Problemstellung

![DevOps-Infinity](https://devopedia.org/images/article/54/7602.1513404277.png)

Services zu betreiben bedeutet, dass sie überwacht werden müssen. Wenn ich nicht weiss, wie es meinem Service geht, kann ich ihn nicht betreuen.

Doch leider ist es nicht ganz so einfach. Denn um mein Service zu überwachen, muss ich erst in Erfahrung bringen, welche Kriterien ich ansetze, um den Zustand des Services beobachten zu können.

Wie geht es Ihnen? Wie ich das meine? Geht es Ihnen gut? Antworten Sie mit "gut", um schwierigen Diskussionen auszuweichen? Antworten Sie mit "gut", weil Sie gerade gar nicht darüber nachdachten und man doch auf diese Frage immer mit "gut" antwortet? Oder denken Sie einen kurzen Moment darüber nach, finden ein paar Sachen, die gerade nicht so gut laufen? Und andere, die wiederum positiv sind und Sie damit eigentlich doch lieber die Antwort "gut" geben?

Nochmals zur Frage zurück: Wie ich das meine? Privat, beruflich, schulisch, Freundeskreis, Gesundheitszustand, Gemütszustand? Die Frage ist doch vielfältig und viel zu ungenau gestellt! Und genau so geht's auch beim Monitoring eines Services.

## Messwerte

Als erstes gilt es heraus zu finden, welche Messwerte denn vorhanden sind. CPU-Leistung, Memory-Auslastung, Disk-IO, Netzwerk-IO sind schnell mal aufgezählt. Doch wenn man näher hinschaut, sind diese vielfältiger: Wenn wir Storage betrachten, müssen wir unterscheiden zwischen Storage-Verbrauch (z.B. relativ zum verfügbaren Speicherplatz) und Storage-Gebrauch, also wie viel in welcher Zeit gelesen bzw. gespeichert wird. Natürlich immer für jeden Storage einzeln.

Vielleicht kennen Sie sowas bereits bei VMs oder auch physischen Rechnern. Doch auf Applikationsebene können noch viel mehr Messpunkte dazu kommen: Anzahl Logins pro Intervall (Zeiteinheit) oder Log-Einträge zu einem bestimmten Thema. Oder Anzahl Threads die für einen bestimmten Job parallel ausgeführt werden. Dann gibt es womöglich Warteschlangen, die überwacht werden können.

Weil die Möglichkeiten zu vielfältig sind, müssen wir bald einschränken. Denn Monitoring benötigt Ressourcen. Einerseits stellt sich die Frage, wie oft Sie diese Daten speichern. Auch wenn der einzelne Wert nur 4 Bytes (32 bit Integer) benötigt, so würden, sollten Sie diesen sekündlich erfassen, damit im Jahr 240 MB Daten anfallen (nochmals 4 Bytes für Zeitstempel).  Tönt als noch nicht so viel? Haben Sie 120 solcher Datenpunkte sind wir schon bei rund 14 GB. Im Vergleich: Würden wir die Datenpunkte im 5-Minuten-Takt erfassen (z.B. als Mittelwert), bräuchten wir nur noch 46 MB pro Jahr.

Die zweite Ressource die nicht unerheblich ist: Das abrufen und abspeichern solcher Daten ist mit Traffic und Arbeit verbunden. In dieser Zeit kann der Service seine eigentliche Aufgabe nicht erfüllen bzw. die Kosten des Monitorings steigen. Das ist unerwünscht.

## Erfassen

Zur Erfassung der Daten gibt es zwei wesentliche Prinzipien:

1. Poll: Die Zentrale fragt jedes Element nach dem Kriterium: Geht es dir gut? Wie steht's um dein Puls? Musst zur Toilette? Hast du Hunger?
2. Push: Das Element meldet sich von sich aus: Es tut mir weh, bitte hilf mir!

Zwar wäre die zweite Variante oft effizienter, doch leider ist manchmal der Service nicht mehr fähig, sich zu melden. Dies aus mehreren Gründen: Überlastung, Freeze, Exit.

Betrachten Sie an dieser Stelle die [Funktionsweise des Protokolls SNMP](https://de.wikipedia.org/wiki/Simple_Network_Management_Protocol#Funktionsweise). Welche Nachrichtentypen entsprechen welchem Prinzip? Zählen Sie je vier Messpunkte / Kriterien zu den beiden Prinzipien auf.

<!--
Folgender Abschnitt ist vorerst obsolet geworden.

## Aufbau einer Infrastruktur

Ein häufig eingesetzter Monitoring-Stack ist *node_exporter* &rarr; *prometheus* &rarr; *grafana*.

Erstellen Sie mit folgendem Cloud-Init eine Umgebung im MaaS:
```cloud-init
Packages:
  - docker-compose
RunCmd:
  - git clone https://github.com/vegasbrianc/prometheus.git
  - cd prometheus
  - docker-compose up
```
Vielleicht wollen Sie sich noch einen SSH-Zugang verschaffen? Ergänzen Sie einfach das Cloud-Init...

!!! task "Schau genau hin"
    Haben Sie das jetzt wirklich gemacht?! Sinnvollerweise haben Sie vorher recherchiert, was Sie da machen! Tja, falls nicht, müssen Sie das jetzt nachholen. Und dann geht's darum, herauszufinden, was das jetzt brachte und was Sie damit anstellen können.
-->