# 52 AWS CloudWatch

## AWS CloudWatch

AWS bietet ein einheitliches System für die Überwachung aller Dienste in ihrer Cloud: *CloudWatch*. Cooler Nebeneffekt: Dieser Dienst ist grundsätzlich kostenlos nutzbar. Das heisst, Sie können Regeln und Aktionen definieren. Erst wenn das Abrufinterval kleiner als 5 Minuten sein soll ergeben sich Kosten. Und natürlich kosten alle Dienste, die Sie womöglich automatisch anwerfen.

An sich ist der Service relativ einfach: Man bestimmt Regeln und definiert dazu passend Aktionen.

Regeln können sein:

- Das arithmetische Mittel der CPU-Leistung einer EC2-Instanz der letzten 5 Minuten > 50 %
- Ein Storage ist über 80 % voll
- Ein bestimmter Log-Eintrag taucht auf

Aktionen können sein:

- Starte weitere Instanzen (max 5)
- Sende eine E-Mail an den Administrator
- Sende eine SMS an den Administrator
- Setze eine Firewall-Regel, um den Zugang zu blockieren

Das macht jetzt natürlich neugierig.

## Aufgabe 1 Speicherplatz in S3-Bucket
Erstellen Sie einen S3-Bucket. Setzen Sie in CloudWatch eine Regel, in der Sie den Bucket überwachen und dafür sorgen, dass Sie eine E-Mail kriegen, wenn der Speicherplatz des Buckets über 50 MB gefüllt wird. Füllen Sie dann den Bucket mit 51 MB Daten.

## Aufgabe 2 CPU-Last einer EC2-Instanz
Bauen Sie eine EC2-Instanz mit folgendem Script:

```bash
#!/bin/bash
sleep 10
$0 &
$0 &
```

Setzen Sie eine Regel in CloudWatch, um die CPU-Last der Instanz zu beobachten. Finden Sie eine Aktion, die sinnvoll wäre, mit dieser Problematik umzugehen?

## Aufgabe 3 Log-Eintrag
Die erste Frage lautet: Was bedeutet diese Metrik? Wie können Sie mittels Log-Einträge überhaupt CloudWatch triggern? Womöglich hilft Ihnen der Befehl `logger`, den Sie im M122 schon einmal angetroffen haben. Lösen Sie aus einer EC2-Instanz das Ereignis aus und setzen Sie eine geeignete Aktion.