# 31 CI/CD-Pipelines

!!! abstract "Zielsetzung"

    Sie verstehen die Elemente einer CI/CD-Pipeline. Sie automatisieren den Container-Build und nutzen dazu einen eigenen *Gitlab Runner*.

!!! task "Projektbasis"
    In diesem Kapitel arbeiten Sie mit dem eigenen Fork der *RefCard-01*. Wenn Sie unten Ihren eigenen Runner bauen, nutzen Sie deshalb direkt dieses bestehende Projekt.

CI/CD (Continuous Integration/Continuous Delivery) sind bewährte DevOps-Methoden zur Automatisierung in der Anwendungsentwicklung. Die Hauptkonzepte von CI/CD sind *Continuous Integration* (kontinuierliche Integration), *Continuous Delivery* und *Continuous Deployment* (kontinuierliche Verteilung).

## Pipeline-Phasen

Die Schritte in einer CI/CD-Pipeline stellen verschiedene Untergruppen von Aufgaben dar, die in sogenannte Pipeline-Phasen eingeteilt werden. Zu diesen Phasen gehören üblicherweise:

- *Build*: Die Phase, in der die Anwendung kompiliert wird.
- *Test*: Die Phase, in der der Code getestet wird. Hier lassen sich durch Automatisierung sowohl der Zeit- als auch der Arbeitsaufwand verringern.
- *Release*: Die Phase, in der die Anwendung ins Repository gestellt wird.
- *Bereitstellung*: In dieser Phase wird der Code in der Produktionsumgebung bereitgestellt.
- *Validierung und Compliance*: Welche Schritte zur Validierung eines Builds notwendig sind, bestimmen die Anforderungen des jeweiligen Unternehmens.

Diese Liste der Pipeline-Phasen ist nicht abschliessend. So werden Sie Ihre eigene Pipeline an den Anforderungen Ihres Unternehmens und der Applikation ausrichten müssen.

!!! task "Schau genau hin"
    Sie finden bei Atlassian einen spannenden Artikel zur [DevOps-Pipeline](https://www.atlassian.com/de/devops/devops-tools/devops-pipeline). Verfügen Sie über übrige zeitliche Ressourcen, könnte sich die Lektüre der restlichen Artikel durchaus lohnen. Das Autorenkollektiv befasst sich eingehend mit der DevOps-Kultur, die der Bezeichnung der Fachrichtung "Plattform-Entwickler" zugrunde liegt.

## Pipelines in GitLab

In einem ersten Schritt setzen Sie in GitLab eine Pipeline für die Phasen *Build* und *Release* um. Das Ziel ist die automatisierte Kompilierung und Bereitstellung des Container-Images direkt aus GitLab.

In GitlLab besteht eine Pipeline aus zwei Komponenten:

- *Jobs*: Definieren das *was*. Ein Job kann beispielsweise kompilieren oder Code testen.
- *Stages*: Definieren *wann* ein Job ausgeführt wird. Eine Stage B, die Tests ausführt, wird nach der Stage A, welche kompiliert, ausgeführt.

![Pipelines example](./assets/gitlab-stages.png)

## Eigener Runner

Damit Sie Ihren persönlichen GitLab-Account nicht mit einer Kreditkarte verifizieren müssen (was nötig wäre, um die offiziellen GitLab-Runner als Ressourcen für die Builds zu nutzen), setzen Sie einen GitLab-Runner bei sich lokal in Docker auf.
Erstellen Sie einen Verweis zu Ihrem eigenen GitLab-Runner unter *Settings* - *CI/CD* - *Runners*. Stellen Sie sicher, dass die *Shared Runners* für Ihre Pipeline nicht genutzt werden und erstellen Sie dann einen neuen Project runner.

![image-20230510084245151](./assets/gitlab-runner-create1.png)

![image-20230510084245154](./assets/gitlab-runner-create2.png)

Nach dem Erstellen des Runners erhalten Sie einen Token für diesen. Kopieren Sie den Token in den unten stehenden Befehl und führen Sie alle bei sich lokal in der Shell aus.

```bash
docker run -d --name gl-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner

docker exec gl-runner gitlab-runner register \
  --url https://gitlab.com \
  --token HIER_IHREN_TOKEN_EINFUEGEN --executor docker \
  --docker-image docker:23.0.4 --docker-privileged --non-interactive

docker restart gl-runner
```

Verifizieren Sie danach, dass GitLab und Ihr lokaler Runner verbunden sind:

![image-20230510084245152](./assets/gitlab-runner-connected.png)

Alternativ können Sie auch das *MaaS* als Rechenmaschine benutzen. Folgendes CloudInit-File setzt Ihnen eine VM mit dem Runner auf. Sie müssen einzig das Token noch an der richtigen Stelle einsetzen.

```yml title="runner.yml"
--8<-- "docs/ci-cd/cloudinit-runner.yml"
```

!!! task "Schau genau hin"
    Dieses CloudInit erstellt keinen User. Damit kriegen Sie keinen Zugriff auf die VM. Natürlich können Sie einen solchen hinzufügen. Aber eigentlich brauchen Sie diesen nicht: Haben Sie etwas Vertrauen &mdash; nach *deployed* braucht es einfach noch einen Moment, bis docker installiert und der Container *up & running* ist.

## Simple GitLab Pipeline

Wir starten mit einer ganz einfachen Pipeline, um die Funktionalität zu testen.

Erstellen Sie in Ihrem Fork des *Ref-Card-01* Projekts die Datei `.gitlab-ci.yml` mit folgendem Inhalt.

```yaml title=".gitlab-ci.yml"
test-job:
  script:
    - echo "This is my first job!"
    - date
```

Commiten und pushen Sie diese Datei zu GitLab. Sobald Sie erfolgreich gepusht haben, finden Sie in GitLab jeweils unter *CI/CD* &rarr; *Pipelines* Ihre eigene Pipeline, welche sogleich von einem GitLab Runner ausgeführt wird.

![image-20230510084245512](./assets/gitlab-pipeline.png)

Erkunden Sie diese Pipeline und schauen Sie sich auch das Job-Log an. Recherchieren Sie dazu auch nach weiterführenden Erklärungen zu diesem Vorgang. Wenn Sie sich sattelfest fühlen und das Grundkonzept verstanden haben, fahren Sie mit dem nächsten Schritt weiter.

!!! success "Lernerfolg"
    Sie haben einen eigenen Runner an gitlab verhängt und können nun von gitlab automatisiert Rechenzeit auslösen. Die Tasks sind abhängig vom Inhalt der `.gitlab-ci.yml`. Dabei sind kaum Grenzen gesetzt.