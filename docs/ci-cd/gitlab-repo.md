# 32 Gitlab Container Registry

!!! abstract "Zielsetzung"
    Sie nehmen in diesem Kapitel die gitlab Container Registry in Betrieb und lernen, das builden eines Images per `.gitlab-ci.yml` zu automatisieren.

## Einleitung

Für diese Übung verwenden Sie wieder Ihr eigenes *nginx/index.html* Projekt. Sie haben darin ein `Dockerfile` erstellt, durch welches Sie mit dem vorhandenen Quellcode automatisiert ein lauffähiges *Docker Image* erstellen können. Diese Arbeit wiederholt sich, sobald Sie am Quellcode (`index.html`) etwas ändern. Manuelle Wiederholungen wollen wir aber vermeiden.

Gitlab bietet zu jedem Projekt automatisch eine Container Registry an. Also ein Ort, wo wir Images ablegen können. Diese gehören schliesslich nicht in den Code! Sie finden die Registry unter `Deploy` &rarr; `Container Registry`. Momentan ist diese leer.

## Variables

Im folgenden Script werden vordefinierte Variablen eingesetzt, die von GitLab zur Verfügung stehen. Sie finden eine [Referenz in der GitLab Dokumentation](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html). Der Einsatz dieser Variablen ermöglicht, dass sich der GitLab Runner ohne Interaktion durch Benutzende in die nötige Registry-Umgebung anmelden kann.

## Aufgabe

Der Plan lautet nun, automatisiert bei Code Änderungen das Image neu zu erstellen und in der *Gitlab Container Registry* bereit zu halten. Dazu nutzen wir unseren früher erstellten `Gitlab Runner` und steuern diesen über die Datei `.gitlab-ci.yml`:

1. Erstellen Sie die `.gitlab-ci.yml`:
    ```yml title=".gitlab-ci.yml"
    --8<-- "nginx/.gitlab-ci.yml"
    ```

1. Passen Sie den Teil des Scripts an

    Dazu müssen Sie den Anweisungen von gitlab folgen (im Bereich `Deploy`&mdash;`Container Registry`). Hoffentlich benutzen Sie 2FA, so dass Sie auch noch das Handling mit dem *Application Token* einbeziehen müssen.

    !!! task "Schau genau hin"
        Der Login erfolgt im Teil *before_script*. Sie werden auch erfolgreich sein, wenn Sie diese Zeile in den Teil *script* einfügen (wäre das ein Versuch wert?). Welchen Sinn wird hier mit dem Ansatz *before_script* verfolgt?

1. Aktualisieren Sie Ihr Projekt auf gitlab

1. Ändern Sie die index.html, committen Sie korrekt und pushen Sie!

!!! success "Ergebnissicherung"
    Sie finden bei erfolgreicher Durchführung in der Container Registry Ihres Projekts das fertig erstellte Image.