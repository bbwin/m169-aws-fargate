# 11 Docker CLI

## Einleitung

Falls Sie mit den Grundlagen und dem Anwendungszweck von Docker noch nicht vertraut sind, bietet sich die [Einführung von Docker](https://docs.docker.com/get-started/overview/) selbst an. Für uns ist wichtig, dass Sie folgende Grafik verstehen.

![Docker Architektur](https://docs.docker.com/get-started/images/docker-architecture.webp)

!!! info "Docker Container"
    Der Container ist ein Prozess (mit Subprozessen), welcher durch ein Image erstellt wird. Container basieren auf [Linux Namespaces](https://en.wikipedia.org/wiki/Linux_namespaces) &mdash; einer Technologie in Linux, welche abgeschottete Laufzeitumgebunden erlaubt. Ein Container enthält kein Betriebssystem, weshalb er nicht mit einer virtuellen Maschine verwechselt werden darf.
    
    Container isolieren die Laufzeitumgebungen von Applikationen. Beim Start eines Containers werden Konfigurationen, Umgebungsvariablen, CPU-, Netzwerk- und Speicheroptionen mitgegeben. Die Freiheiten innerhalb eines Containers werden exakt auf die konfigurierten Ressourcen beschränkt.

!!! info "Docker Images"
    Docker Images enthalten die Informationen / Ressourcen, die für die Ausführung eines Containers nötig sind. Insbesondere das Filesystem, aber auch Zusatzinformationen wie den Entrypoint sind im Image enthalten.

    Docker Images werden in Registries bereitgestellt. Docker selbst bietet *Docker Hub* als eine solche Registry zur Verfügung. Häufig macht es für KMUs Sinn, selbst eine Registry oder zumindest ein Spiegel/Proxy zu betreiben.

!!! info "Dockerfile"
    Das Dockerfile beschreibt als Textdatei (*YaML*), wie ein (oder mehrere) *Docker Image* erstellt werden soll. Dabei wird typischerweise auf ein bestehendes Image aufgesetzt. Da Docker Images mehrere Schichten enthalten, können daraus lange Verkettungen entstehen &mdash; was selten erwünscht ist. Ordnung zu halten ist also wichtig!

## Steuerung durch die CLI

Auch wenn grafische Tools für die Bedienung von Docker existieren, so sind diese in produktiven Umgebungen unerwünscht. Profis wissen, was sie tun &mdash; wir wollen, dass auch Sie Profi werden! Deshalb nehmen wir uns Zeit für die Commands. Diese werden inbesondere auch zur Automatisation (und darauf zielt die ganze Übung hin) benötigt.

Grundsätzlich wird Docker über den Command `docker` gesteuert. Da (leider) meistens Docker unter root ausgeführt wird, müssen Sie in Ubuntu-Umgebungen `sudo` davor setzen bzw. vorher (z.B. mit `sudo bash`) root werden. Besser wäre es [Docker rootless](https://docs.docker.com/engine/security/rootless/) zu betreiben. So oder so: Wir verzichten fortan auf den Hinweis und überlassen die Lösung Ihnen.

Generelle Syntax: `docker <topic> <command> <options>`

### Container verwalten

Idealerweise verschaffen Sie sich erst mal eine Übersicht über alle laufenden Container. Womöglich haben Sie noch alte Container aus anderen Modulen in Betrieb. Für die Steuerung der *Container* ist der Command `container` zuständig. Weil das Auflisten der laufenden Container so häufig ist, wird auch per `docker ps` eine Abkürzung angeboten.

!!! task "Container"
    Lassen Sie sich mit `docker container -h` anzeigen, welche Commands zur Verfügung stehen.

    - Worin unterscheidet sich `create` von `run`?
    - Welche Informationen zeigt `stats`?
    - Wozu könnte `rm` benötigt werden? Was bewirkt `prune`?
    - Zeichnen Sie ein State Diagramm der Zustände von Containern[!](https://miro.medium.com/v2/resize:fit:2258/format:webp/1*vca4e-SjpzSL5H401p4LCg.png)

### Images verwalten

Ebenfalls wichtig ist das Management von *Images*.

!!! task "Images"
    Verschaffen Sie sich wiederum eine Übersicht über `docker image -h`. Ein paar Aufgaben:

    - Welche Images liegen auf Ihrem Gerät (ungenutzt) herum?
    - Wieviel Speicher belegen diese?
    - Wo wird der Speicherplatz benutzt? Wie kann man das ändern?
    - Welche Images benötigen Sie tatsächlich noch?
    - Welche Konsequenzen erleiden Sie, wenn Sie diese löschen?
    - Welche Informationen erlangen Sie durch `inspect` und `history`?
    - Wozu gibt es bei `prune` noch ein `-a`? Vergleichen Sie die Ergebnisse (Reihenfolge ist relevant!)!

## Aufgaben

Es ist einfach, einen Systemtest durchzuführen. Docker bietet dafür ein `hello-world`-Image an. Sie können es direkt per `docker run hello-world` starten.

Wow, tatsächlich. Wenn Docker das Image noch nicht lokal verfügbar hat, lädt es dieses selbständig ab dem Repository (wahrscheinlich `hub.docker.com`) und führt es anschliessend aus. Wollen Sie es erst lokal zur Verfügung stellen, nutzen Sie stattdessen `docker pull hello-world` und erst später den Command `run`.

```
$ docker run hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
c1ec31eb5944: Pull complete 
Digest: sha256:6352af1ab4ba4b138648f8ee88e63331aae519946d3b67dae50c313c6fc8200f
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```

Sie können den ausführenden Container im Task Manager suchen (unter Linux mit `top` oder `ps axf`). Wieviel Ressourcen verbraucht er?

Versuchen Sie mit dem vorgeschlagenen Befehl in eine Ubuntu Umgebung wechseln und sich umsehen. Mit `exit` kommen Sie wieder aus dem Container.

!!! task "Schau genau hin"
    Schauen Sie mal nach: Läuft `hello-world` noch immer. Nein? Starten Sie's erneut. Was berichtet `docker ps`? Und was meint `docker ps -a`? Und was bedeutet das nun? Und wie reagieren Sie?

!!! success "Zusammenfassung"
    Docker ist cool, Docker bietet wichtige Vorteile. Aber Docker ist nicht perfekt. Oder vielleicht gibt es gute Gründe, weshalb Docker so ist, wie's ist? Zur Zeit nehmen wir's einfach einmal zur Kenntnis!

    Sie lernten in diesem Kapitel mit Docker Befehlen Container und Images zu bedienen. Docker bietet natürlich noch viel mehr...