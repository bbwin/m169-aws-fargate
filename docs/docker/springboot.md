# 13 Spring Boot App

!!! abstract "Lerninhalt"
    Statt einer plumpen HTML-Seite builden wir eine Spring Boot Applikation, die in einem schlanken Container zur Ausführung gebracht wird.

## Problembeschreibung

Unser neues Objekt ist eine Spring Boot Applikation, welche jedoch ebenfalls eine einfache statische Website ausspuckt. Immerhin wird eine IP-Adresse dynamisch generiert und dargestellt.

In einem neuen Verzeichnis clonen wir die RefCard-01:

```sh
cd ~
git clone https://gitlab.com/bbwrl/m347-ref-card-01
cd m347-ref-card-01
```

Zwei Objekte sind für uns in diesem Verzeichnis interessant: Die `pom.xml` erklärt [maven](https://maven.apache.org/), was es mit `src` anstellen muss, damit es eine Webapplikation daraus produzieren kann. Diese wird später mit [Java](https://www.java.com/de/) zur Ausführung gebracht.

Um *Maven* die Applikation bilden zu lassen, benötigen wir ein [Java Development Kit (JDK)](https://www.oracle.com/ch-de/java/technologies/downloads/#java17). In der `pom.xml` finden Sie den Hinweis, dass JDK in der Version 17 benutzt werden muss.

Jetzt ist offensichtlich, dass wir keine Lust haben, auf unseren wertvollen eigenen BYOD einen Wildwuchs von Java-Versionen zu betreiben. Das ergäbe garantiert Probleme mit anderen Java-Applikationen, die andere Versionen beanspruchen. Gerade Java ist relativ hartnäckig mühsam, was Versionierung betrifft. Hier macht es besonders viel Sinn, die Applikation in einer isolierten Umgebung zu builden und auch zu betreiben, um Abhängigkeiten zu anderen Applikationen zu vermeiden.

## Dockerfile: integrierte Lösung

Wir erstellen also ein Dockerfile, um die Applikation darin zu builden und dann die fertige Applikation auszuführen:

```dockerfile title="Dockerfile"
--8<-- "ref-card-01/Dockerfile.v1"
```

Wir können jetzt diese Applikation wie folgt up & running kriegen:

```sh
docker build -t app:latest .
docker run -p 8080:8080 app
```

!!! success "Erfolg"
    Die Applikation spuckt nun, falls Sie mit Ihrem Browser auf [localhost:8080](http://localhost:8080/) gehen, die gewünschte Website aus. Natürlich gilt es `localhost` gemäss Ihrer Umgebung zu ersetzen.

!!! task "Schau genau hin"
    Die Webapplikation zeigt eine IP-Adresse an. Welche? Zeichnen Sie ein Netzwerkschema!

## Ressourcen-Verschwendung

Wir untersuchen nun mit `docker image ls -a` das Ergebnis-Image:

```sh
REPOSITORY TAG     IMAGE ID       CREATED     SIZE
app        latest  95ab6257c528   ..... ago   416MB
```

Wenn wir vom Szenario ausgehen, dass wir dieses Image weltweit bei Bedarf z. B. in einem Kubernetes Cluster aufpoppen und wieder eliminieren wollen, sind über 400 MB für eine statische Website etwas unglaublich viel! Wir sollten es im Platzbedarf massiv reduzieren.

Die erste Idee könnte lauten: Zwar benötigen wir für den Build der Webapplikation Maven, Eclipse, JDK, etc. Aber für die Ausführung reicht ein einfaches Java Runtime Environment (JRE), welches von sich aus schon einmal viel kleiner ausfällt. Schlanker heisst auch weniger sicherheitsanfällig und performanter.

## Dockerfile: Build / Run getrennt

Wir bilden deshalb den Prozess auch im Dockerfile so ab: Wir bauen einen Container, in welchem wir die Applikation builden und übertragen dann das Ergebnis-Artefakt in einen neuen Container, der für den Betrieb der Applikation zuständig ist.

Somit sieht das neue Dockerfile wie folgt aus:

```dockerfile title="Dockerfile"
--8<-- "ref-card-01/Dockerfile.v2"
```

Beachten Sie, wie wir das erste Image benennen (`FROM ... as ...`) und später bei der Instruktion `COPY` mit `--from=` darauf verweisen.

!!! task "Schau genau hin"
    Haben Sie die Instruktion `EXPOSE` entdeckt? Ersetzt dieses das `-p` bei `docker run`? Lesen Sie den Abschnitt in der [Docker Reference](https://docs.docker.com/reference/dockerfile/#expose)! Insbesondere lesen Sie genau, was `-P` betrifft! Probieren Sie's auch einmal aus!

Das Ergebnis ist jetzt noch nicht extrem gut. Kriegen Sie das Ergebnis-Image noch kleiner?

!!! success "Lernerfolg"
    In einem Dockerfile können Sie nun mehrere Container untereinander abhängig gestalten. Sie haben erkannt, wie ein Build-Prozess vom Betrieb getrennt wird.