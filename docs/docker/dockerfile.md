# 12 Dockerfile

## Webserver-Container mit Nginx und statischer Website

Starten Sie ein Terminal und erstellen Sie sich in Ihrem Homeverzeichnis ein neues Verzeichnis, worin Sie sich rein bewegen:

```sh
mkdir -p ~/webserver
cd ~/webserver
```

Wir benötigen eine statische Website. Erstellen Sie dazu eine Datei `index.html`:
```html title="index.html"
--8<-- "nginx/index.html"
```

Unser Webserver-Container wird nun durch ein *Dockerfile* bestimmt. Ein *Dockerfile* definiert die Schritte zur Erzeugung eines neuen Images.

Erstellen Sie die Datei `Dockerfile` (Gross-/Kleinschreibung beachten, keine Dateiendung)!

```dockerfile title="Dockerfile"
--8<-- "nginx/Dockerfile"
```

!!! task "Schau genau hin"
    Suchen Sie auf [hub.docker.com](https://hub.docker.com) nach dem Basis-Image. Welche Distribution liegt dem Image zugrunde? Sie verwenden womöglich ein anderes Linux. Gibt das nicht ein Problem, da doch kein neuer Kernel betrieben wird (Container statt VM)?

    Zudem wird die `index.html` in einen Ordner innerhalb des Containers kopiert. Wäre nicht `/var/www/html/` der passende Ordner?

Als erster Arbeitsschritt prüfen wir `docker build` und starten daraus einen mit Etikett (Tag) versetzten Container:

```sh
docker build -t nginx-custom:latest .
```

Überprüfen Sie Ihr erstelltes Image mit den bereits bekannten Befehlen `images` oder `inspect`. Lassen Sie anschliessend Ihr Image mit `docker run` laufen und stellen Sie eine fehlerfreie Ausführung sicher. Nginx läuft standardmässig auf *tcp-Port 80*. Erweitern Sie den Docker-Befehl mit den Anweisungen, um diesen Port auf Ihrem Gerät verfügbar zu machen. Konsultieren Sie die [Nginx Docker Hub](https://hub.docker.com/_/nginx)-Dokumentation, falls Sie die Befehle nicht kennen oder nutzen Sie `sudo docker run --help` (Tipp: *publish* ist das Stichwort, welches Sie suchen).

![Website in grün](./assets/website-green.png)

!!! task "Repetition"
    Jetzt möchten Sie die Hintergrund-Farbe der Website auf *blau* verändern. Was tun Sie, um diesen Wunsch zu erfüllen und welche Schritte sind nötig, damit die Website in blau erscheint?

    Zeichnen Sie den Vorgang schematisch!

!!! success "Zusammenfassung"
	In einem für diesen Auftrag neu erstellten Verzeichnis haben Sie die zwei Dateien `Dockerfile` und `index.html` erstellt. Daraus haben Sie sich ein Docker Image erstellt und erfolgreich im Browser getestet.