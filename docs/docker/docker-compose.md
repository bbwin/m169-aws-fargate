# 14 Multi-Container-Applikation

!!! abstract "Lerninhalt"
    Wir bauen uns die Jokes-DB (RefCard-03) containerisiert auf.

## Problembeschreibung

Das eine ist der Build eines Images (dafür kennen wir nun das `Dockerfile`). Das andere ist die Ausführung der Container. Der Command `docker run ...` beinhaltet bald einige Parameter, die es zu dokumentieren gilt. Ganz IaC-Like brauchen wir eine YaML-Datei: `docker-compose.yml`!

Es existiert selbstverständlich eine [Compose file version 3 reference](https://docs.docker.com/compose/compose-file/compose-file-v3/).

Für die Ref-Card-03 Jokes-Datenbank-Springboot-Applikation haben wir die Basis eines `docker-compose.yml` für Sie bereitgestellt:

```yml title="docker-compose.yml"
--8<-- "https://gitlab.com/bbwrl/m346-ref-card-03/-/raw/main/docker-compose.yml?ref_type=heads"
```

Dieses beinhaltet die MariaDB und eine Container-Instanz mit PHPmyAdmin. Letztere ist von Ersterer abhängig.

!!! task "Schau genau hin"
    Nehmen Sie sich etwas Zeit und schreiben Sie zu jeder Zeile einen kurzen Kommentar. Bei `volumes` sollten Sie zudem sich etwas mehr Zeit nehmen und die Auswirkungen dieses Teils studieren!

## Aufgabe

1. Forken Sie das Projekt [RefCard-03](https://gitlab.com/bbwrl/m346-ref-card-03/) und clonen sich das Ergebnis. 

1. Das Projekt beinhaltet `pom.xml` und `src`. Äquivalent zur RefCard-01 builden Sie die Applikation entsprechend der Situation im Vorkapitel. Sie benötigen dafür ein `Dockerfile`.

    !!! warning "Java-Version"

        Beachten Sie die benötigte Java-Version für den Build. Sie finden diese in der pom.xml verzeichnet.

    !!! warning "Java-Version upgraden"

        Seit September 2023 ist Java 11 EOL. Der Entwickler der Jokes-DB behauptet jedoch, dass die Applikation auch mit Java Version 17 funktionieren würde. Passen Sie also die Version im `pom.xml` an und schauen Sie, ob der Entwickler recht behält. Vorteil: Sie können dann die gleichen Images verwenden wie bei der `RefCard-01`.

1. Im README finden Sie Hinweise, wie diese Applikation, sobald builded, gestartet werden kann. Nutzen Sie diese, um die `docker-compose.yml` zu ergänzen.

1. Sie haben das Projekt *geforkt*, damit Sie es sich später wieder ins gitlab *pushen* können. Womöglich gelingt Ihnen dieser Schritt erst nach dem nächsten Kapitel.

!!! success "Lernerfolg"
    Sie haben mit `Docker-Compose` eine (eigentlich zwei) Applikation mit 3-Tier-Architektur in Betrieb genommen.