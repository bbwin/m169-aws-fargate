# M169 &mdash; Docker und AWS Fargate

!!! abstract "Einleitung"
    Dieses Lernmaterial unterstützt Sie darin, Docker und AWS Fargate zu erkunden. Virtuelle Maschinen sind für viele Anwendungen viel zu schwergewichtig und ressourcenintensiv. Mit Containern können wir isolieren, Ressourcen bewusst einsetzen und *Infrastructure as Code* nutzen. Diese Container lassen sich in verschiedenen Umgebungen, in unserem Fall in AWS Fargate, ausführen.

Sie werden durch diese Themen durchgeführt.

1. [Docker](docker/index.md)
1. [Versionsverwaltung](versioning/index.md)
1. [CI/CD-Pipeline](ci-cd/index.md)
1. [Fargate](fargate/index.md)
1. [DevOps](devops/index.md)

!!! info "Arbeitsumgebung"

    Wir arbeiten mit Linux ohne Desktop-Umgebung (CLI). Dazu gibt es viele Möglichkeiten:

    1. Windows mit WSL2 und Ubuntu Subsystem
    1. Per Dual Boot auf Ihrem persönlichen BYOD
    1. Sie benutzen sowieso ein unixoides Betriebssystem (Mac, Linux)
    1. Virtuelle Maschine auf Ihrem persönlichen BYOD (VirtualBox, multipass, Hyper-V, vmware, ...)
    1. Virtuelle Maschine auf dem [MaaS](https://maas.bbw-it.ch/)
    1. ec2-Instanz im *AWS Learner Lab* oder natürlich eigenem *AWS Account*
    1. Sie mieten einen vhost (in Deutschland für knapp 3 € / Mt erhältlich)

    Welche Variante Sie benutzen, überlassen wir Ihnen. Lassen Sie sich womöglich von Ihrer erfahrenen Lehrperson beraten. Der Autor bevorzugt Variante MaaS, VirtualBox oder falls bereits möglich ec2.

Wir wünschen viel Spass!

*BBW Cluster Plattformentwicklung*
